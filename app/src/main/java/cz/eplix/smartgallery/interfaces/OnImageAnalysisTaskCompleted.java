package cz.eplix.smartgallery.interfaces;

import com.microsoft.projectoxford.vision.contract.AnalysisResult;

/**
 * Created by Tomas.Falta on 16.02.2018.
 */

public interface OnImageAnalysisTaskCompleted {
    void onImageAnalysisTaskCompleted(AnalysisResult result);
}
