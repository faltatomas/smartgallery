package cz.eplix.smartgallery.helpers;

import android.support.media.ExifInterface;
import android.text.TextUtils;

/**
 * Created by Tomas.Falta on 18.02.2018.
 */

public class ExifHelper {
    public static int getExifRotation(String imgPath) {
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(imgPath);
            String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (!TextUtils.isEmpty(rotationAmount)) {
                int rotationParam = Integer.parseInt(rotationAmount);
                switch (rotationParam) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        rotate = 0;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                }
            }
        } catch (Exception e) {

        }

        return rotate;
    }

}
