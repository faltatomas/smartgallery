package cz.eplix.smartgallery.helpers;

import com.microsoft.projectoxford.face.contract.Emotion;
import com.microsoft.projectoxford.face.contract.Face;

/**
 * Created by Tomas.Falta on 09.03.2018.
 */

public class FaceHelper {
    public static String getFaceMainEmotion(Face face) {
        Emotion emotion = face.faceAttributes.emotion;

        double nMax = -1;
        String sResult = "";

        if(emotion.anger > nMax){
            nMax = emotion.anger;
            sResult = "Anger";
        }
        if(emotion.contempt > nMax){
            nMax = emotion.contempt;
            sResult = "Contempt";
        }
        if(emotion.disgust > nMax){
            nMax = emotion.disgust;
            sResult = "Disgust";
        }
        if(emotion.fear > nMax){
            nMax = emotion.fear;
            sResult = "Fear";
        }
        if(emotion.happiness > nMax){
            nMax = emotion.happiness;
            sResult = "Happiness";
        }
        if(emotion.neutral > nMax){
            nMax = emotion.neutral;
            sResult = "Neutral";
        }
        if(emotion.sadness > nMax){
            nMax = emotion.sadness;
            sResult = "Sadness";
        }
        if(emotion.surprise > nMax){
            sResult = "Surprise";
        }

        return sResult;
    }
}
