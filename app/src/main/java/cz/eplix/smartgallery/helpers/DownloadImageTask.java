package cz.eplix.smartgallery.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class DownloadImageTask extends AsyncTask<String, Void, byte[]> {
    public DownloadImageTask() {
    }

    protected byte[] doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap bmp = null;
        byte[] byteArray = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            bmp = BitmapFactory.decodeStream(in);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
            bmp.recycle();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return byteArray;
    }

}