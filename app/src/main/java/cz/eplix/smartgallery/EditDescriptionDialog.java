package cz.eplix.smartgallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Tomas.Falta on 09.03.2018.
 */

public class EditDescriptionDialog extends DialogFragment {

    public interface EditDescriptionListener {
        public void onEditDescriptionSaveClick(EditDescriptionDialog dialog);
      //  public void onDialogNegativeClick(DialogFragment dialog);
    }

    private String description;

    public String getDescription() {
        return description;
    }

    EditDescriptionListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            listener = (EditDescriptionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EditKeywordsListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle mArgs = getArguments();
        final String receivedDescription = mArgs.getString("description");
        Log.d("Received desc to:", receivedDescription);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final EditText editText = new EditText(getActivity());
        editText.setText(receivedDescription);

        builder.setMessage(R.string.edit_description)
                .setView(editText)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        description = editText.getText().toString();
                        listener.onEditDescriptionSaveClick(EditDescriptionDialog.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditDescriptionDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

}
