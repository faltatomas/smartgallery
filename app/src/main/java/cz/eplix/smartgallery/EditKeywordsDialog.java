package cz.eplix.smartgallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import java.util.logging.Logger;

/**
 * Created by Tomas.Falta on 09.03.2018.
 */

public class EditKeywordsDialog extends DialogFragment {

    public interface EditKeywordsListener {
        public void onEditKeywordsSaveClick(EditKeywordsDialog dialog);
      //  public void onDialogNegativeClick(DialogFragment dialog);
    }

    private String keywords;

    public String getKeywords() {
        return keywords;
    }

    EditKeywordsListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            listener = (EditKeywordsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EditKeywordsListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle mArgs = getArguments();
        final String receivedKeywords = mArgs.getString("keywords");
        Log.d("Received keywords to:", receivedKeywords);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final EditText editText = new EditText(getActivity());
        editText.setText(receivedKeywords);

        builder.setMessage(R.string.edit_keywords)
                .setView(editText)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        keywords = editText.getText().toString();
                        listener.onEditKeywordsSaveClick(EditKeywordsDialog.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditKeywordsDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

}
