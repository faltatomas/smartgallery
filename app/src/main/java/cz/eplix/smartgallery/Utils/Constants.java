package cz.eplix.smartgallery.Utils;

/**
 * Created by Tomas.Falta on 13.02.2018.
 */

public class Constants {
    public static final String PHOTO_ID = "photo_id";
    public static final String PHOTO = "photo";
    public static final String PHOTO_NAME = "photo_name";
    public static final String PHOTOS_TO_MAP = "photos_to_map";
public static final String SELECTED_LOCATION = "photo_selected_location";

public static final int SET_MAP_LOCATION_INTENT = 1;


    public static final String ExifDescriptionTag = "UserComment";
}
