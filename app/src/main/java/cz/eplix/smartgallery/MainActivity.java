package cz.eplix.smartgallery;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.model.LatLng;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.CustomVisionTrainingManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.TrainingApi;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.Trainings;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Project;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.CustomVisionPredictionManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.PredictionEndpoint;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.adapters.PhotosAdapter;
import cz.eplix.smartgallery.helpers.BitmapHelper;
import cz.eplix.smartgallery.models.MapPhotoItem;
import cz.eplix.smartgallery.models.PhotoItem;
import cz.eplix.smartgallery.models.PhotoItemParceable;
import cz.eplix.smartgallery.services.TrainingAPI;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PhotosAdapter.OnPhotoClickListener, SearchView.OnQueryTextListener {
    private ArrayList<PhotoItem> photoList;
    private PhotosAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.run_camera);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Intent callIntent = new Intent(Intent.ACTION_CAMERA_BUTTON);
                //startActivity(callIntent);
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.photos_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        reloadPhotos();

        recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            final Snackbar snackBar = Snackbar.make(drawer, getString(R.string.dashboard_activity_snackbar_text), Snackbar.LENGTH_LONG);
            snackBar.setAction(getString(R.string.dashboard_activity_snackbar_action), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);


        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        adapter.filter(query);
        recyclerView.setAdapter(adapter);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adapter.filter(query);
        recyclerView.setAdapter(adapter);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_load_tags) {

            return true;
        } else if (id == R.id.action_load_faces) {

            return true;
        } else if (id == R.id.action_showAllOnMap) {
            Intent intent = new Intent(getApplicationContext(), PhotosOnMapActivity.class);
            intent.putParcelableArrayListExtra(Constants.PHOTOS_TO_MAP, getPhotosToMap());
            startActivity(intent);
        } else if(id == R.id.action_reload_photos){
            reloadPhotos();
        }

        return super.onOptionsItemSelected(item);
    }

    private void reloadPhotos(){
        ArrayList<File> directories = new ArrayList<>();
        directories.add(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
        directories.add(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM));

        photoList = loadPhotosInDirectories(directories);


        adapter = new PhotosAdapter(getApplicationContext(), photoList, this);
        recyclerView.setAdapter(adapter);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivity(intent);
        }else if (id == R.id.nav_learnCV){
            Intent intent = new Intent(getApplicationContext(), CustomVisionActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private ArrayList<MapPhotoItem> getPhotosToMap() {
        ArrayList<MapPhotoItem> mapPhotos = new ArrayList<>();

        for (PhotoItem item : photoList) {
            if (item.getLatLng() != null) {
                mapPhotos.add(new MapPhotoItem(item.getLatLng(), item.getName(), item.getName(), item.getPath()));
            }
        }
        return mapPhotos;
    }

    private ArrayList<PhotoItem> loadPhotosInDirectories(ArrayList<File> directories) {
        ArrayList<PhotoItem> photos = new ArrayList<>();

        for (File directory : directories) {
            File[] files = directory.listFiles(new ImageFileFilter());
            if(files != null){
                for (File file : files) {

                    PhotoItem photo = new PhotoItem(file.getName(), file.getAbsolutePath());
                    photos.add(photo);
                }
            }
        }

        Collections.sort(photos);

        return photos;
    }

    @Override
    public void onClick(PhotoItem photo) {
        Intent intent = new Intent(getApplicationContext(), PhotoDetailActivity.class);
        intent.putExtra(Constants.PHOTO, new PhotoItemParceable(photo.getName(), photo.getPath()));
        startActivity(intent);
    }

    private class ImageFileFilter implements FileFilter {

        @Override
        public boolean accept(File file) {
            return isFileImage(file.getName());
        }


        private boolean isFileImage(String filePath) {
            return filePath.toLowerCase().endsWith("jpg") || filePath.toLowerCase().endsWith("png") || filePath.toLowerCase().endsWith("jpeg");
        }
    }
}
