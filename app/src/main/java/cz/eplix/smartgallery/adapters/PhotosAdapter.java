package cz.eplix.smartgallery.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;
import com.microsoft.projectoxford.face.contract.Face;

import java.util.ArrayList;

import cz.eplix.smartgallery.R;
import cz.eplix.smartgallery.helpers.BitmapHelper;
import cz.eplix.smartgallery.helpers.FaceHelper;
import cz.eplix.smartgallery.models.PhotoItem;

/**
 * Created by Tomas.Falta on 12.02.2018.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {


    private ArrayList<PhotoItem> galleryList;
    private ArrayList<PhotoItem> origGalleryList;

    private Context context;
    private OnPhotoClickListener onPhotoClickListener;
    private RequestManager glide;


    public PhotosAdapter(Context context, ArrayList<PhotoItem> galleryList, OnPhotoClickListener onPhotoClickListener) {
        this.galleryList = galleryList;
        this.origGalleryList = new ArrayList<PhotoItem>();
        origGalleryList.addAll(galleryList);

        this.context = context;
        this.onPhotoClickListener = onPhotoClickListener;

        glide = Glide.with(context);
    }

    public void filter(String text) {

        galleryList.clear();
        if(text.isEmpty()){
            galleryList.addAll(origGalleryList);
        } else {
            text = text.toLowerCase();
            for (PhotoItem item : origGalleryList) {
                boolean bAdd = false;


                if (item.getPhotoDescription().getDescription() != null) {
                    if (item.getPhotoDescription().getDescription().toLowerCase().contains(text)) {
                        bAdd = true;
                    }
                }

                if (item.getPhotoDescription().getKeywords() != null) {
                    for (String key : item.getPhotoDescription().getKeywords()) {
                        if (key.toLowerCase().contains(text)) {
                            bAdd = true;
                            break;
                        }
                    }
                }

                if(item.getPhotoDescription().getFaces() != null){
                    for(Face face : item.getPhotoDescription().getFaces()){
                        if(FaceHelper.getFaceMainEmotion(face).toLowerCase().contains(text)){
                            bAdd = true;
                            break;
                        }
                    }
                }

                if(item.getPhotoDescription().getFaces() != null){
                    for(Face face : item.getPhotoDescription().getFaces()){
                        if(face.faceAttributes.gender.toLowerCase().contains(text)){
                            bAdd = true;
                            break;
                        }
                    }
                }


                if(bAdd)
                    galleryList.add(item);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotosAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Log.d("URL", galleryList.get(i).getPath());

        GlideHelper.loadImage(glide, galleryList.get(i).getPath(), viewHolder.img);

        viewHolder.img.setImageBitmap(viewHolder.img.getDrawingCache());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoClickListener.onClick(galleryList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //private TextView title;
        private ImageView img;

        public ViewHolder(View view) {
            super(view);

        //    title = (TextView) view.findViewById(R.id.title);
            img = (ImageView) view.findViewById(R.id.img);
        }
    }


    public interface OnPhotoClickListener {
        void onClick(PhotoItem photo);
    }


    private static class GlideHelper {
        public static void loadImage(RequestManager glide, String url, ImageView view) {
            glide.load(url).apply(new RequestOptions().centerCrop())
                    .into(view);
        }
    }
}