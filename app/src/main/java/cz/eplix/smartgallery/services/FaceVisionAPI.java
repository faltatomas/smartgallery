package cz.eplix.smartgallery.services;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceClient.FaceAttributeType;

import com.microsoft.projectoxford.face.rest.ClientException;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;
import com.microsoft.projectoxford.vision.contract.Category;
import com.microsoft.projectoxford.vision.contract.Face;
import com.microsoft.projectoxford.vision.contract.Tag;
import com.microsoft.projectoxford.vision.rest.VisionServiceException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.eplix.smartgallery.interfaces.OnFaceAnalysisTaskCompleted;
import cz.eplix.smartgallery.interfaces.OnImageAnalysisTaskCompleted;

/**
 * Created by Tomas.Falta on 16.02.2018.
 */
public class FaceVisionAPI extends AsyncTask<String, String, String> {

    private StringBuilder sb;
    private OnFaceAnalysisTaskCompleted listener;

    private Bitmap mBitmap;
    private FaceServiceClient client;

    // Store error message
    private Exception e = null;

    public FaceVisionAPI(Bitmap mBitmap, FaceServiceClient client, StringBuilder sb, OnFaceAnalysisTaskCompleted listener) {
        this.mBitmap = mBitmap;
        this.client = client;
        this.sb = sb;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... args) {
        try {
            return process();
        } catch (Exception e) {
            this.e = e;    // Store error
        }

        return null;
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);
        // Display based on error existence

        if (e != null) {
            sb.append("Error: " + e.getMessage());
            this.e = null;
        } else {
            Gson gson = new Gson();
            com.microsoft.projectoxford.face.contract.Face[] faces = gson.fromJson(data, com.microsoft.projectoxford.face.contract.Face[].class);

         //  sb.append("Image format: " + result.metadata.format + "\n");
         //  sb.append("Image width: " + result.metadata.width + ", height:" + result.metadata.height + "\n");

         //  for (Tag tag : result.tags) {
         //      sb.append("Tag: " + tag.name + ", confidence: " + tag.confidence + "\n");
         //  }

         //  for (Category category : result.categories) {
         //      sb.append("Category: " + category.name + ", score: " + category.score + "\n");
         //  }

         //  for (Caption caption : result.description.captions) {
         //      sb.append("Description: " + caption.text + ", confidence: " + caption.confidence+ "\n");
         //  }
         //  sb.append("\n");
         //  int faceCount = 0;
         //  for (Face face : result.faces) {
         //      faceCount++;
         //      sb.append("face " + faceCount + ", gender:" + face.gender + "(score: " + face.genderScore + "), age: " + +face.age + "\n");
         //      sb.append("    left: " + face.faceRectangle.left + ",  top: " + face.faceRectangle.top + ", width: " + face.faceRectangle.width + "  height: " + face.faceRectangle.height + "\n");
         //  }
         //  if (faceCount == 0) {
         //      sb.append("No face is detected");
         //  }
         //  sb.append("\n");

         //  sb.append("\nDominant Color Foreground :" + result.color.dominantColorForeground + "\n");
         //  sb.append("Dominant Color Background :" + result.color.dominantColorBackground + "\n");

         //  sb.append("\n--- Raw Data ---\n\n");
         //  sb.append(data);

         //  Log.d("VisionAPI result", sb.toString());

            listener.onFaceAnalysisTaskCompleted(faces);
        }

        //listener.onAnalysisTaskCompleted(null);
    }

    private String process() throws VisionServiceException, IOException, ClientException {
        Gson gson = new Gson();
        FaceAttributeType[] types = new FaceAttributeType[]{
                FaceAttributeType.Age,
                FaceAttributeType.Accessories,
                FaceAttributeType.Blur,
                FaceAttributeType.Emotion,
                FaceAttributeType.Exposure,
                FaceAttributeType.FacialHair,
                FaceAttributeType.Gender,
                FaceAttributeType.Glasses,
                FaceAttributeType.Hair,
                FaceAttributeType.HeadPose,
                FaceAttributeType.Makeup,
                FaceAttributeType.Noise,
                FaceAttributeType.Occlusion,
                FaceAttributeType.Smile};

        // Put the image into an input stream for detection.
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

        com.microsoft.projectoxford.face.contract.Face[] faces = this.client.detect(inputStream, true, true, types);


        String result = gson.toJson(faces);
        Log.d("result", result);

        return result;
    }

}
