package cz.eplix.smartgallery.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.StrictMode;

import com.bumptech.glide.Glide;
import com.google.common.io.ByteStreams;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.CustomVisionPredictionManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.PredictionEndpoint;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.ImagePrediction;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.Prediction;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.CustomVisionTrainingManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.TrainingApi;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.Trainings;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Image;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.ImageFileCreateBatch;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.ImageFileCreateEntry;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Iteration;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Project;
import com.microsoft.azure.cognitiveservices.search.imagesearch.BingImageSearchAPI;
import com.microsoft.azure.cognitiveservices.search.imagesearch.BingImageSearchManager;
import com.microsoft.azure.cognitiveservices.search.imagesearch.models.ImageObject;
import com.microsoft.azure.cognitiveservices.search.imagesearch.models.ImagesModel;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Region;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Tag;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import cz.eplix.smartgallery.R;
import cz.eplix.smartgallery.helpers.DownloadImageTask;

public class TrainingAPI  {
    private TrainingApi _trainClient;
    private PredictionEndpoint _predictClient;
    private BingImageSearchAPI _bingClient;
    private Context _context;

    private String projectName = "SMAPProject";
    private UUID projectId = UUID.fromString("8eb3168e-06f8-4488-a893-716369ef7188");

    public TrainingAPI(String sCustomVisionTrainingKey, String sCustomVisionPredictionKey, String sBingImageKey, Context currentContext){

        _trainClient = CustomVisionTrainingManager.authenticate("https://northeurope.api.cognitive.microsoft.com/customvision/v3.0/Training/", sCustomVisionTrainingKey);
        _predictClient = CustomVisionPredictionManager.authenticate("https://northeurope.api.cognitive.microsoft.com/customvision/v2.0/Prediction/",sCustomVisionPredictionKey);
        _bingClient = BingImageSearchManager.authenticate(sBingImageKey);
        _context = currentContext;

        // override NetworkOnMainThreadException :D
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public Project CreateNewProject(){

        Trainings trainer = _trainClient.trainings();
        Project project = trainer.createProject()
                .withName(projectName)
                .execute();

        return project;
    }

    public void RemovePicturesFromModel(){
        Trainings trainer = _trainClient.trainings();

        List<Image> images = trainer.getTaggedImages().withProjectId(projectId).execute();

        List<String> imageIds = new ArrayList<>();

        for (Image img: images) {
            imageIds.add(img.id().toString());
        }

        trainer.deleteImages(projectId, imageIds);
    }

    public void AddPhoto(String fileName, byte[] content, String sPhotoTag){
        Trainings trainer = _trainClient.trainings();
        Project project = LoadProject();

        List<Tag> tags = trainer.getTags().withProjectId(project.id()).execute();
        Tag photoTag = null;

        for (Tag tag: tags) {
            if(tag.name().equals(sPhotoTag)){
                photoTag = tag;
            }
        }

        if(photoTag == null){
            photoTag = trainer.createTag()
                    .withProjectId(project.id())
                    .withName(sPhotoTag)
                    .execute();
        }

        AddImageToProject(trainer, project, fileName, content, photoTag.id(),null );
    }

    public void LearnModel(ImagesModel models, String keyword) {
        Trainings trainer = _trainClient.trainings();
        Project project = null;

        try{
            project = LoadProject();}
        catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        Tag hemlockTag = trainer.createTag()
                .withProjectId(project.id())
                .withName(keyword)
                .execute();

        int nCount =  models.value().size();

        nCount = nCount > 10 ? 10 : nCount;

        for (int i = 0; i < nCount; i++) {

            ImageObject img = models.value().get(i);

            String fileName = keyword + "_" + i + ".jpg";
            byte[] contents = GetImage(img.contentUrl());
            AddImageToProject(trainer, project, fileName, contents, hemlockTag.id(), null);
        }

        RunTraining(trainer, project);
    }

    public ImagesModel BindImagesFromBing(String sKeyword){
        int nCount= 50;

        ImagesModel imageResults = _bingClient.bingImages().search()
                .withQuery(sKeyword)
                .withMarket("en-us")
                .execute();

        return imageResults;

    }

    public ImagePrediction PredictImage(byte[] image){
        Project project = LoadProject();


        ImagePrediction results = _predictClient.predictions().predictImage()
                .withProjectId(project.id())
                .withImageData(image).withIterationId(UUID.fromString("53797e2a-97b6-4db1-9ac4-55be04fabb78"))
                .execute();

        return results;
    }

    private Project LoadProject(){
        Trainings trainer = _trainClient.trainings();
        Project project = trainer.getProject(projectId);

        return project;
    }

    private void RunTraining(Trainings trainer, Project project){

        System.out.println("Training...");
        Iteration iteration = trainer.trainProject(project.id());

        while (iteration.status().equals("Training"))
        {
            System.out.println("Training Status: "+ iteration.status());
            try {
                Thread.sleep(1000);
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            iteration = trainer.getIteration(project.id(), iteration.id());
        }
        System.out.println("Training Status: "+ iteration.status());
        trainer.updateIteration(project.id(), iteration.id(), iteration.withIsDefault(true));
    }

    private void AddImageToProject(Trainings trainer, Project project, String fileName, byte[] contents, UUID tag, double[] regionValues)
    {
        System.out.println("Adding image: " + fileName);
        ImageFileCreateEntry file = new ImageFileCreateEntry()
                .withName(fileName)
                .withContents(contents);

        ImageFileCreateBatch batch = new ImageFileCreateBatch()
                .withImages(Collections.singletonList(file));

        // If Optional region is specified, tack it on and place the tag there, otherwise
        // add it to the batch.
        if (regionValues != null)
        {
            Region region = new Region()
                    .withTagId(tag)
                    .withLeft(regionValues[0])
                    .withTop(regionValues[1])
                    .withWidth(regionValues[2])
                    .withHeight(regionValues[3]);
            file = file.withRegions(Collections.singletonList(region));
        } else {
            batch = batch.withTagIds(Collections.singletonList(tag));
        }

        trainer.createImagesFromFiles(project.id(), batch);
    }

    private byte[] GetImage(String sUrl)
    {
        try {
          return DownloadImageFromUrl(sUrl);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    private byte[] DownloadImageFromUrl(String sUrl) throws ExecutionException, InterruptedException {

        return new DownloadImageTask().execute(sUrl).get();
    }
}


