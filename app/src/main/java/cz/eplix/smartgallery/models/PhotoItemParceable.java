package cz.eplix.smartgallery.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tomas.Falta on 09.03.2018.
 */

public class PhotoItemParceable implements Parcelable {
    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public PhotoItemParceable(String name, String path) {
        this.name = name;
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.path);
    }

    public PhotoItemParceable() {
    }

    protected PhotoItemParceable(Parcel in) {
        this.name = in.readString();
        this.path = in.readString();
    }

    public static final Parcelable.Creator<PhotoItemParceable> CREATOR = new Parcelable.Creator<PhotoItemParceable>() {
        @Override
        public PhotoItemParceable createFromParcel(Parcel source) {
            return new PhotoItemParceable(source);
        }

        @Override
        public PhotoItemParceable[] newArray(int size) {
            return new PhotoItemParceable[size];
        }
    };
}
