package cz.eplix.smartgallery.models;

import android.support.media.ExifInterface;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.helpers.FileHelper;
import cz.eplix.smartgallery.helpers.GPS;

/**
 * Created by Tomas.Falta on 12.02.2018.
 */

public class PhotoItem implements Comparable<PhotoItem>  {

    private LatLng latLng;
    private String name;
    private String path;
    private ExifPhotoDescription photoDescription;

    private String dateCreated;
    private String dimensions;
    private String size;

    private String cameraModel;
    private String aperture;

    public String getSize() {
        return size;
    }

    public String getShutterSpeed() {
        return shutterSpeed;
    }

    public String getISO() {
        return ISO;
    }

    private String shutterSpeed;
    private String ISO;

    private Date photoDateCreated;

    public Date getPhotoDateCreated() {
        return photoDateCreated;
    }

    public String getName() {
        return name;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }



    public String getDateCreated() {
        return dateCreated;
    }

    public String getDimensions() {
        return dimensions;
    }


    public String getCameraModel() {
        return cameraModel;
    }

    public String getAperture() {
        return aperture;
    }



    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ExifPhotoDescription getPhotoDescription() {
        return photoDescription;
    }

    public void setPhotoDescription(ExifPhotoDescription photoDescription) {
        this.photoDescription = photoDescription;
    }

    //public Bitmap getImageSource(){
    //     return BitmapHelper.decodeBitmapFromFile(path, 500, 500);
    //}


    @Override
    public int compareTo(PhotoItem o) {
        return getPhotoDateCreated().compareTo(o.getPhotoDateCreated());
    }

    public PhotoItem(String name, String path) {
        this.name = name;
        this.path = path;

        loadExifValues();
    }

    private void loadExifValues() {
        size = FileHelper.getFileSize(new File(path));

        ExifInterface exifInt = getExifInterface();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        String serializedValue = exifInt.getAttribute(Constants.ExifDescriptionTag);

        if (serializedValue != null && serializedValue.length() > 0) {
            try {
                photoDescription = gson.fromJson(serializedValue, ExifPhotoDescription.class);
            } catch (Exception e) {
                photoDescription = new ExifPhotoDescription();
            }
        } else {
            photoDescription = new ExifPhotoDescription();
        }

        dateCreated = exifInt.getAttribute(ExifInterface.TAG_DATETIME);

        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");

        try {
            photoDateCreated = simpleDateFormat.parse(dateCreated);
        } catch (Exception e) {
            photoDateCreated = new Date();
        }

        dimensions = exifInt.getAttribute(ExifInterface.TAG_IMAGE_WIDTH) + " x " + exifInt.getAttribute(ExifInterface.TAG_IMAGE_LENGTH);

        cameraModel = exifInt.getAttribute(ExifInterface.TAG_MODEL);
        aperture = exifInt.getAttribute(ExifInterface.TAG_APERTURE_VALUE);
        shutterSpeed = getExposureTime(exifInt); // formatExposureTime(Double.valueOf(exifInt.getAttribute(ExifInterface.TAG_SHUTTER_SPEED_VALUE)));
        ISO = exifInt.getAttribute(ExifInterface.TAG_PHOTOGRAPHIC_SENSITIVITY);

        loadLatLng(exifInt);
    }


    private String getExposureTime(final ExifInterface exif)
    {
        String exposureTime = exif.getAttribute(ExifInterface.TAG_EXPOSURE_TIME);

        if (exposureTime != null)
        {
            exposureTime = formatExposureTime(Double.valueOf(exposureTime));
        }

        return exposureTime;
    }

    private String formatExposureTime(final double value) {
        String output;

        if (value < 1.0f) {
            output = String.format(Locale.getDefault(), "%d/%d", 1, (int) (0.5f + 1 / value));
        } else {
            final int integer = (int) value;
            final double time = value - integer;
            output = String.format(Locale.getDefault(), "%d''", integer);

            if (time > 0.0001f) {
                output += String.format(Locale.getDefault(), " %d/%d", 1, (int) (0.5f + 1 / time));
            }
        }

        return output;
    }

    public void saveLocation(LatLng newCoords) {
        ExifInterface exifInt = getExifInterface();

        if (newCoords == null) {
            exifInt.setAttribute(ExifInterface.TAG_GPS_LATITUDE, "0/0,0/0000,00000000/00000");
            exifInt.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "0");
            exifInt.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, "0/0,0/0,000000/00000");
            exifInt.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "0");


        } else {
            exifInt.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GPS.convert(newCoords.latitude));
            exifInt.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(newCoords.latitude));
            exifInt.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(newCoords.longitude));
            exifInt.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(newCoords.longitude));
        }
        try {
            exifInt.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadLatLng(exifInt);
    }

    private void loadLatLng(ExifInterface exifInt) {
        float[] latLon = new float[2];
        if (exifInt.getLatLong(latLon) && latLon[0] != 0 && latLon[1] != 0) {
            latLng = new LatLng(latLon[0], latLon[1]);
        }
        else{
            latLng = null;
        }
    }

    public void saveExifValues() {
        ExifInterface exifInt = getExifInterface();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        exifInt.setAttribute(Constants.ExifDescriptionTag, gson.toJson(photoDescription));

        try {
            exifInt.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private ExifInterface getExifInterface() {

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (exif == null) {
            throw new NullPointerException("Exif interface is null");
        }

        return exif;
    }
}
