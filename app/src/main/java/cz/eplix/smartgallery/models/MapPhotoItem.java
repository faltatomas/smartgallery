package cz.eplix.smartgallery.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Tomas.Falta on 06.03.2018.
 */

public class MapPhotoItem implements ClusterItem, Parcelable {
    private final LatLng position;
    private final String title;
    private final String snippet;
    private final String path;
    private Bitmap photoSource;

    public MapPhotoItem(LatLng coords, String title, String snippet, String imagePath) {
        position = coords;
        this.title = title;
        this.snippet = snippet;
        this.path = imagePath;
        this.photoSource = null;
    }

    public Bitmap getPhotoSource() {
        return photoSource;
    }

    public void setPhotoSource(Bitmap photoSource) {
        this.photoSource = photoSource;
    }

    public String getPath() {
        return path;
    }



    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.position, flags);
        dest.writeString(this.title);
        dest.writeString(this.snippet);
        dest.writeString(this.path);
    }

    protected MapPhotoItem(Parcel in) {
        this.position = in.readParcelable(LatLng.class.getClassLoader());
        this.title = in.readString();
        this.snippet = in.readString();
        this.path = in.readString();
        this.photoSource = null;
    }

    public static final Creator<MapPhotoItem> CREATOR = new Creator<MapPhotoItem>() {
        @Override
        public MapPhotoItem createFromParcel(Parcel source) {
            return new MapPhotoItem(source);
        }

        @Override
        public MapPhotoItem[] newArray(int size) {
            return new MapPhotoItem[size];
        }
    };
}


