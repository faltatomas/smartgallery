package cz.eplix.smartgallery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.helpers.BitmapHelper;
import cz.eplix.smartgallery.helpers.MultiDrawable;
import cz.eplix.smartgallery.models.MapPhotoItem;
import cz.eplix.smartgallery.models.PhotoItem;
import cz.eplix.smartgallery.models.PhotoItemParceable;

public class PhotosOnMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        ClusterManager.OnClusterClickListener<MapPhotoItem>, ClusterManager.OnClusterInfoWindowClickListener<MapPhotoItem>, ClusterManager.OnClusterItemClickListener<MapPhotoItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MapPhotoItem> {

    private GoogleMap mMap;

    private ClusterManager<MapPhotoItem> mClusterManager;

    private ArrayList<MapPhotoItem> mapPhotos;

    public GoogleMap getMap(){
        return mMap;
    }

    private class PhotoRenderer extends DefaultClusterRenderer<MapPhotoItem>{
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;
        private final RequestManager glide;

        public PhotoRenderer(){
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiPhoto = getLayoutInflater().inflate(R.layout.multi_photo, null);
            mClusterIconGenerator.setContentView(multiPhoto);
            mClusterImageView = (ImageView) multiPhoto.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_photo_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_photo_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);

            glide = Glide.with(getApplicationContext());


        }

        @Override
        protected void onBeforeClusterItemRendered(MapPhotoItem photo, MarkerOptions markerOptions) {
            mImageView.setImageBitmap(photo.getPhotoSource());

            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(photo.getTitle());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MapPhotoItem> cluster, MarkerOptions markerOptions) {

            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (MapPhotoItem p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;

                Drawable d2 = new BitmapDrawable(getResources(), BitmapHelper.decodeBitmapFromFile(p.getPath(), 100, 100));

                profilePhotos.add(d2);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);

            mClusterImageView.setImageDrawable(multiDrawable);
            mClusterIconGenerator.setColor(Color.WHITE);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.set_photo_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent resultIntent = new Intent();

        switch (item.getItemId()) {

            default:

                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();

                return true;

        }
    }

    @Override
    public boolean onClusterClick(Cluster<MapPhotoItem> cluster) {
        // Show a toast with some info when the cluster is clicked.
        String firstName = cluster.getItems().iterator().next().getTitle();
    //    Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MapPhotoItem> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(MapPhotoItem item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MapPhotoItem item) {
        Intent intent = new Intent(getApplicationContext(), PhotoDetailActivity.class);
        intent.putExtra(Constants.PHOTO, new PhotoItemParceable(item.getTitle(), item.getPath()));
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_on_map);

        mapPhotos = getIntent().getParcelableArrayListExtra(Constants.PHOTOS_TO_MAP);

        for(final MapPhotoItem photo : mapPhotos){
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(photo.getPath())
                    .into(new SimpleTarget<Bitmap>(200, 200) {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            photo.setPhotoSource(resource);
                        }
                    });
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setUpClusterer() {
        // Position the map.
        mClusterManager = new ClusterManager<MapPhotoItem>(this, getMap());
        mClusterManager.setRenderer(new PhotoRenderer());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {
        if(mapPhotos != null  && mapPhotos.size() > 0) {
            mClusterManager.addItems(mapPhotos);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapPhotos.get(0).getPosition(), 4));

        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        setUpClusterer();

    }
}
