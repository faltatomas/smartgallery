package cz.eplix.smartgallery;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.microsoft.azure.cognitiveservices.search.imagesearch.models.ImagesModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import cz.eplix.smartgallery.services.TrainingAPI;

public class CustomVisionActivity extends AppCompatActivity {

    private TrainingAPI _trainingApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_vision);

        _trainingApi = new TrainingAPI(getString(R.string.CustomVisionTrainingKey), getString(R.string.CustomVisionPredictionKey), getString(R.string.BingSearchKey), getApplicationContext());

        Button clickButton = (Button) findViewById(R.id.btnLearnModelFromBing);
        clickButton.setOnClickListener( new View.OnClickListener() {

           @Override
           public void onClick(View v) {
               EditText tbKeyword = (EditText) findViewById(R.id.tbKeyword);

               if(tbKeyword.getText().length() >0){
                   View parentLayout = findViewById(android.R.id.content);

                   String sKeyword = tbKeyword.getText().toString();
                   ImagesModel results = null;
                   try{
                       results =  _trainingApi.BindImagesFromBing(sKeyword);


                       Snackbar.make(parentLayout, "Binded photos from bing", Snackbar.LENGTH_LONG).show();

                   }
                   catch (Exception exc){
                        String s = exc.getMessage();
                   }

                   if(results == null){
                       return;
                   }

                       _trainingApi.LearnModel(results, sKeyword);
                   Snackbar.make(parentLayout, "Model learnend", Snackbar.LENGTH_LONG).show();


               }
           }
       });

        Button removePhotos = (Button) findViewById(R.id.btnClearModel);
        removePhotos.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                    try{
                       _trainingApi.RemovePicturesFromModel();

                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar.make(parentLayout, "Photos removed", Snackbar.LENGTH_LONG).show();

                    }
                    catch (Exception exc){
                        String s = exc.getMessage();
                    }
                }
        });

    }

    private byte[] DownloadImageFromUrl(String sUrl) throws ExecutionException, InterruptedException {

        return Glide.with(getApplicationContext()).as(byte[].class)
                .load(sUrl).into(100, 100)
                .get();
    }
}
