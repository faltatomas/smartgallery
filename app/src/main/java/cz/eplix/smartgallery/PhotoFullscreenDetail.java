package cz.eplix.smartgallery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.maps.model.LatLng;

import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.models.PhotoItem;
import cz.eplix.smartgallery.models.PhotoItemParceable;

public class PhotoFullscreenDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PhotoItemParceable photoItemParceable = (PhotoItemParceable)getIntent().getParcelableExtra(Constants.PHOTO);
        PhotoItem photo = new PhotoItem(photoItemParceable.getName(), photoItemParceable.getPath());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_photo_fullscreen_detail);

        PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);
        Glide.with(getApplicationContext()).load(photo.getPath()).into(photoView);

        if(getActionBar() != null)
            getActionBar().setTitle(photo.getName());
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle(photo.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            default:
                finish();
                return true;

        }
    }
}
