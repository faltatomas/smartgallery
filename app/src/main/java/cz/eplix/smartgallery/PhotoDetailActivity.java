
package cz.eplix.smartgallery;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.ImagePrediction;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.Prediction;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceRectangle;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;
import com.microsoft.projectoxford.vision.contract.Tag;

import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.helpers.BitmapHelper;
import cz.eplix.smartgallery.helpers.FaceHelper;
import cz.eplix.smartgallery.interfaces.OnFaceAnalysisTaskCompleted;
import cz.eplix.smartgallery.interfaces.OnImageAnalysisTaskCompleted;
import cz.eplix.smartgallery.models.PhotoItem;
import cz.eplix.smartgallery.models.PhotoItemParceable;
import cz.eplix.smartgallery.services.FaceVisionAPI;
import cz.eplix.smartgallery.services.RequestVisionAPI;
import cz.eplix.smartgallery.services.TrainingAPI;

import static cz.eplix.smartgallery.helpers.FaceHelper.getFaceMainEmotion;

public class PhotoDetailActivity extends AppCompatActivity implements OnMapReadyCallback, OnImageAnalysisTaskCompleted, OnFaceAnalysisTaskCompleted, EditKeywordsDialog.EditKeywordsListener, EditDescriptionDialog.EditDescriptionListener {

    @BindView(R.id.pnlDateInfo)
    ConstraintLayout pnlDateInfo;
    @BindView(R.id.lblPhotoDate)
    TextView lblPhotoDate;
    @BindView(R.id.lblPhotoTime)
    TextView lblPhotoTime;

    @BindView(R.id.pnlFileInfo)
    ConstraintLayout pnlFileInfo;
    @BindView(R.id.lblPhotoName)
    TextView lblPhotoName;
    @BindView(R.id.lblPhotoDimensions)
    TextView lblPhotoDimensions;
    @BindView(R.id.lblPhotoSize)
    TextView lblPhotoSize;

    @BindView(R.id.pnlCameraInfo)
    ConstraintLayout pnlCameraInfo;
    @BindView(R.id.lblPhotoCameraName)
    TextView lblPhotoCameraName;
    @BindView(R.id.lblPhotoAperture)
    TextView lblPhotoAperture;
    @BindView(R.id.lblPhotoShutterspeed)
    TextView lblPhotoShutterspeed;
    @BindView(R.id.lblPhotoISO)
    TextView lblPhotoISO;

    @BindView(R.id.pnlDescription)
    ConstraintLayout pnlDescription;
    @BindView(R.id.lblPhotoDescription)
    TextView lblPhotoDescription;

    @BindView(R.id.pnlTags)
    ConstraintLayout pnlTags;
    @BindView(R.id.lblPhotoTags)
    TextView lblPhotoTags;

    @BindView(R.id.pnlFaces)
    ConstraintLayout pnlFaces;
    @BindView(R.id.lblPhotoFaces)
    TextView lblPhotoFaces;

    @BindView(R.id.imgPhoto)
    ImageView imgPhoto;

    private SupportMapFragment mapFragment;

    private GoogleMap mMap;

    private VisionServiceClient client;
    private FaceServiceClient faceClient;
    private PhotoItem photo;
    private PhotoItemParceable photoParceable;

    private Bitmap imageSource;

   private TrainingAPI trainingApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_photo_detail);
        ButterKnife.bind(this);

        if (client == null) {
            client = new VisionServiceRestClient(getString(R.string.VisionAPIKey), getString(R.string.VisionAPIURL));
        }
        if (faceClient == null) {
            try{
                faceClient = new FaceServiceRestClient(getString(R.string.FaceAPIURL), getString(R.string.FaceAPIKey));
            }
            catch (Exception ex){

            }
        }

        trainingApi = new TrainingAPI(getString(R.string.CustomVisionTrainingKey), getString(R.string.CustomVisionPredictionKey), getString(R.string.BingSearchKey), getApplicationContext());

        // get photo from Intent
        photoParceable = (PhotoItemParceable) getIntent().getParcelableExtra(Constants.PHOTO);
        photo = new PhotoItem(photoParceable.getName(), photoParceable.getPath());

        // bind map
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // load photo
        Glide.with(getApplicationContext())
                .asBitmap()
                .load(photo.getPath())
                .into(new SimpleTarget<Bitmap>(500, 500) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        imgPhoto.setImageBitmap(resource);
                        imageSource = resource;

                        if (photo.getPhotoDescription().getKeywords() == null) {
                            analyzeImage(imageSource, client);
                            analyzeFaces(imageSource, faceClient);
                        }
                    }
                });

        imgPhoto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PhotoFullscreenDetail.class);
                intent.putExtra(Constants.PHOTO, photoParceable);
                startActivity(intent);
            }
        });

        // run analyze


        setImageDescriptionText();

        if(getActionBar() != null)
            getActionBar().setTitle(photo.getName());
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle(photo.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.photo_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        View parentLayout = findViewById(android.R.id.content);

        if (id == R.id.action_reload_vision_api) {
            Snackbar.make(parentLayout, R.string.Loading_tags_from_vision_API, Snackbar.LENGTH_LONG).show();
            analyzeImage(imageSource, client);
        } else if (id == R.id.action_reload_face_api) {
            Snackbar.make(parentLayout, R.string.Loading_tags_from_face_API, Snackbar.LENGTH_LONG).show();
            analyzeFaces(imageSource, faceClient);
        } else if (id == R.id.action_highlight_faces) {
            highlightFaces(photo.getPhotoDescription().getFaces());
        } else if (id == R.id.action_set_photo_location) {
            Intent intent = new Intent(getApplicationContext(), SetPhotoLocationActivity.class);
            intent.putExtra(Constants.PHOTO, photoParceable);
            startActivityForResult(intent, Constants.SET_MAP_LOCATION_INTENT);
        } else if (id == R.id.action_edit_keywords) {
            DialogFragment dialog = new EditKeywordsDialog();
            Bundle args = new Bundle();
            args.putString("keywords", TextUtils.join(", ", photo.getPhotoDescription().getKeywords()));
            dialog.setArguments(args);
            dialog.show(getFragmentManager(), "EditKeywordsDialog");

        } else if (id == R.id.action_edit_description) {
            DialogFragment dialog = new EditDescriptionDialog();
            Bundle args = new Bundle();
            args.putString("description", photo.getPhotoDescription().getDescription());
            dialog.setArguments(args);
            dialog.show(getFragmentManager(), "EditDescriptionDialog");

        } else if (id == R.id.action_remove_photo_location) {

            photo.saveLocation(null);

            refreshLocation();
            Snackbar.make(parentLayout, R.string.Photo_location_removed, Snackbar.LENGTH_LONG).show();

        } else if (id == R.id.action_send_to_ml_model){

            try {
                trainingApi.AddPhoto(photo.getName(), org.apache.commons.io.FileUtils.readFileToByteArray(new File(photo.getPath())), photo.getPhotoDescription().getKeywords().get(0));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Snackbar.make(parentLayout, "Photo added to model", Snackbar.LENGTH_LONG).show();

        } else if (id == R.id.action_evaluate_photo_with_ml){
            // evaluate model
            try {
                ImagePrediction results = trainingApi.PredictImage(org.apache.commons.io.FileUtils.readFileToByteArray(new File(photo.getPath())));

                StringBuilder sb = new StringBuilder();

                for (Prediction prediction: results.predictions())
                {
                    sb.append(String.format("\t%s: %.2f%%", prediction.tagName(), prediction.probability() * 100.0f));
                    sb.append("\n");

                    System.out.println(String.format("\t%s: %.2f%%", prediction.tagName(), prediction.probability() * 100.0f));
                }

                Snackbar.make(parentLayout, sb.toString(), Snackbar.LENGTH_LONG).show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.SET_MAP_LOCATION_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                LatLng newLocation = data.getParcelableExtra(Constants.SELECTED_LOCATION);

                photo.saveLocation(newLocation);

                refreshLocation();

                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, R.string.Photo_location_updated_successfully, Snackbar.LENGTH_LONG).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, R.string.Photo_location_not_updated, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void refreshLocation() {
        if (mMap == null)
            return;

        if (photo.getLatLng() != null) {
            mapFragment.getView().setVisibility(View.VISIBLE);
        } else {
            mapFragment.getView().setVisibility(View.GONE);
        }

        if (photo.getLatLng() != null) {
            mMap.clear();

            mMap.addMarker(new MarkerOptions().position(photo.getLatLng()).title(photo.getName()));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(photo.getLatLng()));
        }
    }

    private void setImageDescriptionText() {

        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss");
        DateFormat convertDate = new SimpleDateFormat("MMMM dd, yyyy");
        DateFormat convertTime = new SimpleDateFormat("EEEE, hh:mm");

        Date d = null, d2 = null;
        try {
            d = simpleDateFormat.parse(photo.getDateCreated());
            d2 = simpleDateFormat.parse(photo.getDateCreated());
        } catch (Exception e){
            e.printStackTrace();
        }

        if(d != null && d2 != null){
            pnlDateInfo.setVisibility(View.VISIBLE);
            lblPhotoDate.setText(convertDate.format(d));
            lblPhotoTime.setText(convertTime.format(d2));
        }
        else {
          //  pnlDateInfo.setVisibility(View.GONE);
        }


        lblPhotoName.setText(photo.getName());
        lblPhotoDimensions.setText(photo.getDimensions());
        lblPhotoSize.setText(photo.getSize());

        lblPhotoCameraName.setText(photo.getCameraModel());
        if(photo.getAperture() != null)
            lblPhotoAperture.setText(String.format("f/%s", photo.getAperture()));

        lblPhotoShutterspeed.setText(String.format("%ss", photo.getShutterSpeed()));
        lblPhotoISO.setText(String.format("ISO%s", photo.getISO()));

        if (photo.getPhotoDescription().getDescription() != null) {
            pnlDescription.setVisibility(View.VISIBLE);
            lblPhotoDescription.setText(photo.getPhotoDescription().getDescription());
        } else {
            lblPhotoDescription.setText("No description ...");
           // pnlDescription.setVisibility(View.GONE);
        }

        if (photo.getPhotoDescription().getKeywords() != null) {
            pnlTags.setVisibility(View.VISIBLE);
            lblPhotoTags.setText(TextUtils.join(", ", photo.getPhotoDescription().getKeywords()));
        } else {
            lblPhotoTags.setText("No tags ...");
          //  pnlTags.setVisibility(View.GONE);
        }


        if (photo.getPhotoDescription().getFaces() != null && photo.getPhotoDescription().getFaces().length > 0) {
            pnlFaces.setVisibility(View.VISIBLE);
            StringBuilder sb = new StringBuilder();
            Face[] faces = photo.getPhotoDescription().getFaces();
            for (int i = 0; i < faces.length; i++) {
                Face face = faces[i];
                sb.append("Face #").append(i + 1).append(" Age: ").append(face.faceAttributes.age).append(" Gender: ").append(face.faceAttributes.gender).append(" Emotion: ").append(FaceHelper.getFaceMainEmotion(face));
                sb.append("\n");
            }
            lblPhotoFaces.setText(sb.toString());
        } else {
            lblPhotoFaces.setText("No faces detected ...");
            //pnlFaces.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent intent = new Intent(getApplicationContext(), PhotoLocationActivity.class);
                intent.putExtra(Constants.PHOTO, photo.getLatLng());
                intent.putExtra(Constants.PHOTO_NAME, photo.getName());

                startActivity(intent);
            }
        });

        refreshLocation();
    }

    public void analyzeFaces(Bitmap image, FaceServiceClient faceClient) {
        StringBuilder sb = new StringBuilder();
        try {
            new FaceVisionAPI(image, faceClient, sb, this).execute();
        } catch (Exception e) {
            Log.d("FaceAPI description", e.getMessage());

            View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, R.string.Error_when_loading_data_from_face_API, Snackbar.LENGTH_LONG).show();
        }
    }

    public void analyzeImage(Bitmap image, VisionServiceClient client) {
        StringBuilder sb = new StringBuilder();

        try {
            new RequestVisionAPI(image, client, sb, this).execute();
        } catch (Exception e) {
            Log.d("VisionAPI description", e.getMessage());

            View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, R.string.Error_when_loading_data_from_vision_API, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onImageAnalysisTaskCompleted(AnalysisResult result) {

        List<String> keywords = new ArrayList<>();

        for (Tag tag : result.tags) {
            keywords.add(tag.name);
        }

        photo.getPhotoDescription().setKeywords(keywords);

        List<String> captions = new ArrayList<>();
        for (Caption desc : result.description.captions) {
            captions.add(desc.text);
        }

        photo.getPhotoDescription().setDescription(TextUtils.join(", ", captions));
        photo.saveExifValues();
        setImageDescriptionText();


        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, R.string.Results_from_vision_API_loaded, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onFaceAnalysisTaskCompleted(Face[] faces) {

        highlightFaces(faces);

        photo.getPhotoDescription().setFaces(faces);
        photo.saveExifValues();
        setImageDescriptionText();


        View parentLayout = findViewById(android.R.id.content);

        if (faces != null && faces.length > 0)
            Snackbar.make(parentLayout, getString(R.string.detected) + " " + faces.length + " " + getString(R.string.no_of_faces), Snackbar.LENGTH_LONG).show();
        else
            Snackbar.make(parentLayout, R.string.No_face_detected, Snackbar.LENGTH_LONG).show();
    }

    private void highlightFaces(Face[] faces) {
        Bitmap bitmap = imageSource.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(2);

        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(16 * getResources().getDisplayMetrics().density);

        Paint textStrokekPaint = new Paint();
        textStrokekPaint.setAntiAlias(true);
        textStrokekPaint.setStyle(Paint.Style.STROKE);
        textStrokekPaint.setStrokeWidth(8);
        textStrokekPaint.setColor(Color.BLACK);
        textStrokekPaint.setTextSize(16 * getResources().getDisplayMetrics().density);


        if (faces != null) {
            for (Face face : faces) {
                FaceRectangle faceRectangle = face.faceRectangle;
                canvas.drawRect(
                        faceRectangle.left,
                        faceRectangle.top,
                        faceRectangle.left + faceRectangle.width,
                        faceRectangle.top + faceRectangle.height,
                        paint);

                canvas.drawText(String.valueOf(face.faceAttributes.age), faceRectangle.left, faceRectangle.top - 20, textStrokekPaint);
                canvas.drawText(String.valueOf(face.faceAttributes.age), faceRectangle.left, faceRectangle.top - 20, textPaint);
            }
        }
        imgPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void onEditKeywordsSaveClick(EditKeywordsDialog dialog) {

        String receivedKeywords = dialog.getKeywords();

        String[] newKeywords = receivedKeywords.split(",");
        for (int i = 0; i < newKeywords.length; i++) {
            newKeywords[i] = newKeywords[i].trim();
        }
        photo.getPhotoDescription().setKeywords(Arrays.asList(newKeywords));
        photo.saveExifValues();

        setImageDescriptionText();
    }

    @Override
    public void onEditDescriptionSaveClick(EditDescriptionDialog dialog) {
        String description = dialog.getDescription();
        photo.getPhotoDescription().setDescription(description);
        photo.saveExifValues();
        setImageDescriptionText();

    }
}
