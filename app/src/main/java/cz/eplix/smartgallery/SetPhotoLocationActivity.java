package cz.eplix.smartgallery;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import cz.eplix.smartgallery.Utils.Constants;
import cz.eplix.smartgallery.models.PhotoItem;
import cz.eplix.smartgallery.models.PhotoItemParceable;

public class SetPhotoLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener {

    private GoogleMap mMap;
    private Toolbar toolbar;
    private PhotoItem photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_photo_location);

        PhotoItemParceable photoItemParceable = (PhotoItemParceable)getIntent().getParcelableExtra(Constants.PHOTO);
        photo = new PhotoItem(photoItemParceable.getName(), photoItemParceable.getPath());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(getActionBar() != null)
            getActionBar().setTitle("Set location - " + photo.getName());
        if(getSupportActionBar() != null)
            getSupportActionBar().setTitle("Set location - " + photo.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent resultIntent = new Intent();

        switch (item.getItemId()) {
            case R.id.action_ok:

                LatLng selectedLocation = getSelectedPosition();

                resultIntent.putExtra(Constants.SELECTED_LOCATION, selectedLocation);

                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                return true;

            default:

                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();

                return true;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.set_photo_location, menu);
        return true;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {

        }
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);


        if(photo.getLatLng() != null){
            mMap.addMarker(new MarkerOptions().position(photo.getLatLng()).title(photo.getName()));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(photo.getLatLng()));

        }
    }


    private LatLng getSelectedPosition() {
        return mMap.getCameraPosition().target;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }
}
