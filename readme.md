# SmartGallery

## Základní informace 

**Platforma:** Android

**Programovací jazyk:** Java

**Nástroje a technologie:** AndroidStudio, Azure Cognitive Services - Computer Vision API & Face API, Azure Custom Vision

## Cíl práce

Základním cílem je prozkoumat a popsat možnosti v oblasti Machine Learning pro rozpoznání obsahu fotografií. V rámci práce dojde k naučení vlastního modelu, který bude plněn dvěma různými zdroji dat. Prvním zdrojem dat bude veřejně dostupná služba Bing Image Search. Ta na základě daných klíčových slov použije vizuálně podobné obrázky. Druhým zdrojem budou uživatelské fotografie, které uživatel dle svého uvážení popíše několika klíčovými slovy. Na základě těchto informací dojde k analýze fotografie modelem. Vzhledem ke dvěma odlišným datovým zdrojům pro model bude zajímavé pozorovat případné rozdíly v rozpoznání obrazu i vliv navýšení priority na jeden nebo druhý zdroj. 

## Popis aplikace

Mobilní fotogalerie spravuje fotografie v úložišti mobilního telefonu, nabízí analýzu fotografií pomocí Azure Congnitive Services API, pomocí kterého větou a klíčovými slovy popíše obsah fotografie. V případě, že se na fotografii vyskytují osoby a jejich obličeje, zobrazí pozici obličejů na fotografii a odhadne věk a emoce těchto osob. Navíc nabízí správu lokace a zobrazení fotografií na mapě.

Druhou zásadní funkcionalitou aplikace je komunikace s Microsoft Custom Vision (https://www.customvision.ai), možnost naučení vlastního modelu a odesílání uživatelských fotografií k učení i následnému rozpoznání obrazu. Stejně tak je možné aplikací pomocí poskytnutého API administrátorsky spravovat Custom Vision, vyhledat dle klíčového slova fotografie ve službě Bing Image Search, odeslat je do Custom Vision modelu a následně spustit trénování modelu.


![alt text](http://www.tomasfalta.cz/Upload/fotky/G1.png "SmartGallery 1")
![alt text](http://www.tomasfalta.cz/Upload/fotky/G2.png "SmartGallery 2")
![alt text](http://www.tomasfalta.cz/Upload/fotky/G3.png "SmartGallery 3")

![alt text](http://www.tomasfalta.cz/Upload/fotky/smap.png "SmartGallery 4")

vyhodnocení fotografie pomocí Custom Vision modelu

![alt text](http://www.tomasfalta.cz/Upload/fotky/smap2.png "SmartGallery 5")

webová administrace Custom Vision s trénovacími fotografiemi z vlastní fotogalerie a ze služby Bing Image Search pro tag "people"